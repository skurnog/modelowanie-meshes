#include "parser/ObjParser.h"
#include "parser/PlyParser.h"
#include "mesh/HalfEdgeMesh.h"
#include "mesh/VertexIndexMesh.h"
#include <chrono>

int main(int argc, char **argv) {

//    PlyParser parser;
//    parser.parse("../resources/dummy.ply");
//    parser.parse("../resources/apple.ply");
//    parser.parse("../resources/chopper.ply");
//    parser.parse("../resources/pyramid.ply");
//    parser.test();

    PlyParser parser;
    parser.parse("../resources/cow.ply");

    /********************************************************************************************************
     * Half Edge Mesh
     ********************************************************************************************************/

    HalfEdgeMesh halfEdgeMesh(parser);
    auto start = std::chrono::high_resolution_clock::now();
    auto he_faces = halfEdgeMesh.get_vertex_elements(0);
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "halfEdgeMesh.get_vertex_elements time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_layer_1 = halfEdgeMesh.get_first_layer_of_vertex_surrounding(6);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "halfEdgeMesh.get_first_layer_of_vertex_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_layer_2 = halfEdgeMesh.get_second_layer_of_vertex_surrounding(6);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "halfEdgeMesh.get_second_layer_of_vertex_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_face_layer_1 = halfEdgeMesh.get_first_layer_of_element_surrounding(3);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "halfEdgeMesh.get_first_layer_of_element_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_face_layer_2 = halfEdgeMesh.get_second_layer_of_element_surrounding(3);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "halfEdgeMesh.get_second_layer_of_element_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns" << std::endl;

    //swap edges

    start = std::chrono::high_resolution_clock::now();
    bool has_border = halfEdgeMesh.mesh_contains_border();
    end = std::chrono::high_resolution_clock::now();
    std::cout << "halfEdgeMesh.mesh_contains_border time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    /********************************************************************************************************
     * Vertex Index Mesh
     ********************************************************************************************************/

    VertexIndexMesh vertexIndexMesh(parser);

    start = std::chrono::high_resolution_clock::now();
    auto he_faces_im = vertexIndexMesh.get_vertex_elements(0);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "vertexIndexMesh.get_vertex_elements time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_layer_1_im  = vertexIndexMesh.get_first_layer_of_vertex_surrounding(6);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "vertexIndexMesh.get_first_layer_of_vertex_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_layer_2_im  = vertexIndexMesh.get_second_layer_of_vertex_surrounding(6);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "vertexIndexMesh.get_second_layer_of_vertex_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_face_layer_1_im  = vertexIndexMesh.get_first_layer_of_element_surrounding(3);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "vertexIndexMesh.get_first_layer_of_element_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto he_negh_face_layer_2_im  = vertexIndexMesh.get_second_layer_of_element_surrounding(3);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "vertexIndexMesh.get_second_layer_of_element_surrounding time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    //swap edges

    start = std::chrono::high_resolution_clock::now();
    bool has_border_im  = vertexIndexMesh.mesh_contains_border();
    end = std::chrono::high_resolution_clock::now();
    std::cout << "vertexIndexMesh.mesh_contains_border time: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << "ns"  << std::endl;

    return 0;
}