#ifndef MODELOWANIE_MESHES_HALFEDGE_H
#define MODELOWANIE_MESHES_HALFEDGE_H

struct HE_edge;

struct HE_vertex
{
    unsigned index;
    float x;
    float y;
    float z;

    HE_edge* edge;  // one of the half-edges emantating from the vertex
};

struct HE_face
{
    unsigned id;
    HE_edge* edge = nullptr;  // one of the half-edges bordering the face
};

struct HE_edge
{
    HE_edge* pair = nullptr;   // oppositely oriented adjacent half-edge
    HE_edge* next = nullptr;   // next half-edge around the face
    HE_vertex* vertex = nullptr;   // vertex at the end of the half-edge
    HE_face* face = nullptr;   // face the half-edge borders
};


#endif //MODELOWANIE_MESHES_HALFEDGE_H
