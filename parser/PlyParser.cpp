#include "PlyParser.h"

void PlyParser::parse(std::string file_path)
{
    std::fstream file;
    file.open(file_path, std::ios::in);
    if(!file.good()) throw std::runtime_error("Could not open file");

    std::string line;
    bool header = true;
    unsigned long curr_vert = 0, count_vert = 0;

    while(std::getline(file, line)) {
        if(line == "end_header") {
            header = false;
            continue;
        }

        // process header
        if(header) {
            std::vector<std::string> tokens;
            split(line, tokens);

            if(tokens.at(0) == "element") {
                std::istringstream converter(tokens.at(2));
                int value; converter >> value;
                if(tokens.at(1) == "vertex") count_vert = (unsigned long)value;
            }
        // process data part
        } else {
            std::vector<float> tokens;
            split_num(line, tokens);

            // processing vertices
            if(curr_vert < count_vert) {
                std::array<float, 3> vertex = { tokens.at(0), tokens.at(1), tokens.at(2) };
                vertices.push_back(vertex);
                curr_vert++;
            } else { // processing faces
                std::vector<unsigned> face_elements;
                for(unsigned i = 0; i < tokens.at(0); i++) {
                    face_elements.push_back(tokens.at(i + 1));
                }
                faces.push_back(face_elements);
            }
        }
    }

    generate_halfedge();
}

void PlyParser::test()
{
    std::cout << "VERTICES" << std::endl;
    for(auto& vertex: vertices) {
        std::cout << vertex[0] << " " << vertex[1] << " " << vertex[2] << std::endl;
    }

    std::cout << "FACES" << std::endl;
    for(std::vector<unsigned > face: faces) {
        for(int val: face) {
            std::cout << val << " ";
        }
        std::cout << std::endl;
    }

//    std::cout << "HE_edges len " << HE_edges.size() << std::endl;
//    for(auto &value: HE_edges) {
//        std::cout << "V: " << value.second->vertex->index;
//        if(value.second->next != nullptr) std::cout << ", next: " << value.second->next->vertex->index;
//        if(value.second->pair != nullptr) std::cout << ", pair: " << value.second->pair->vertex->index << "," << value.second->pair->next->vertex->index;
//        else std::cout << ", pair: NONE";
//        if(value.second->face != nullptr) std::cout << ", face: " << value.second->face;
//        std::cout << " edge: " << value.first.first << "," << value.first.second << std::endl;
//    }

//    std::cout << "FACE len " << HE_faces.size() << std::endl;
//    for(auto &value: HE_faces) {
//        std::cout << value->edge->vertex->index << "," << value->edge->next->vertex->index << std::endl;
//    }
}
