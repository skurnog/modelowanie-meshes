#ifndef MODELOWANIE_MESHES_PARSER_H
#define MODELOWANIE_MESHES_PARSER_H

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <array>

#include "HalfEdge.h"

class Parser {
public:
    ~Parser();

    // vertices array
    std::vector<std::array<float, 3>> vertices;
    std::vector<std::vector<unsigned>> faces;

    // half-edge
    std::map<std::pair<int, int>, HE_edge*> HE_edges;
    std::vector<HE_face*> HE_faces;
    std::map<int, HE_vertex*> vertex_pointers;

    virtual void parse(std::string file_path) = 0;
    virtual void test() = 0;

protected:
    void generate_halfedge();

    void split(std::string line, std::vector<std::string>& result, char separator = ' ');
    void split_num(std::string line, std::vector<float>& result);
};

#endif //MODELOWANIE_MESHES_PARSER_H
