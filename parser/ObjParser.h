#ifndef MODELOWANIE_MESHES_VERTEXARRAYOBJPARSER_H
#define MODELOWANIE_MESHES_VERTEXARRAYOBJPARSER_H

#include "Parser.h"

class ObjParser : public Parser {
public:
    void parse(std::string file_path);
    void test();
};

#endif //MODELOWANIE_MESHES_VERTEXARRAYOBJPARSER_H
