#ifndef MODELOWANIE_MESHES_PLYPARSER_H
#define MODELOWANIE_MESHES_PLYPARSER_H

#include "Parser.h"

class PlyParser : public Parser {
public:
    void parse(std::string file_path);
    void test();
};

#endif //MODELOWANIE_MESHES_PLYPARSER_H
