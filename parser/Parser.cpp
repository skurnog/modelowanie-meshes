#include "Parser.h"

Parser::~Parser()
{
    for(HE_face* face: HE_faces) delete face;
//    for(auto& element: HE_edges) {
//        if(element.second->vertex != nullptr) delete element.second->vertex;
//        delete element.second;
//    }

    // FIXME: memory leak, probably not worth it
}

void Parser::generate_halfedge()
{
    for(unsigned i = 0; i < vertices.size(); i++) {
        vertex_pointers[i] = new HE_vertex;
        vertex_pointers[i]->index = i;
        vertex_pointers[i]->x = vertices.at(i)[0];
        vertex_pointers[i]->y = vertices.at(i)[1];
        vertex_pointers[i]->z = vertices.at(i)[2];
        vertex_pointers[i]->edge = nullptr;
    }

    // for every face
    for(auto it = faces.begin(); it != faces.end(); ++it) {
        HE_face *this_face = new HE_face;
        this_face->id = it - faces.begin();

        // for each vertex of face
        auto face = *it;

        // check the direction of vertices
        bool reverse = false;
        for(unsigned i = 0; i < face.size(); i++) {
            if(HE_edges.find(std::make_pair(face.at(i), face.at((i + 1) % face.size()))) != HE_edges.end()) {
                reverse = true;
                break;
            }
        }

        // build structure
        HE_edge *prev_edge = nullptr;
        for(int i = reverse ? face.size() - 1 : 0; reverse ? i >=0 : i < face.size(); reverse ? i-- : i++) {
            unsigned edge_begin_v = face.at(i);
            unsigned edge_end_v = face.at(reverse ? (face.size() + i - 1) % face.size() : (i + 1) % face.size());

            std::pair<int, int> verts_pair(edge_begin_v, edge_end_v);

            HE_edge *curr_edge = new HE_edge;
            HE_edges[verts_pair] = curr_edge;

            // origin vertex
            vertex_pointers[edge_begin_v]->edge = curr_edge;
            curr_edge->vertex = vertex_pointers[edge_begin_v];

            // face
            curr_edge->face = this_face;

            // next halfedge of prev halfedge
            if(prev_edge != nullptr) prev_edge->next = curr_edge;
            prev_edge = curr_edge;

            // first edge in face
            if(i == 0) this_face->edge = curr_edge;

            // last edge in face
            if(i == reverse ? 0 : face.size() - 1) {
                curr_edge->next = HE_edges[std::make_pair(face.at(reverse ? face.size() - 1 : 0), face.at(reverse ? face.size() -2 : 1))];
            }
        }

        for(auto &element: HE_edges) {
            std::pair<int, int> pair_edge(element.first.second, element.first.first);
            if(HE_edges.find(pair_edge) != HE_edges.end()) {
                HE_edges[pair_edge]->pair = element.second;
                element.second->pair = HE_edges[pair_edge];
            }
        }

        HE_faces.push_back(this_face);
    }
}

void Parser::split(std::string line, std::vector<std::string>& result, char separator)
{
    std::stringstream ss(line);
    std::string item;
    while (std::getline(ss, item, separator)) {
        result.push_back(item);
    }
}

void Parser::split_num(std::string line, std::vector<float>& result)
{
    std::istringstream ss(line);
    std::string dummy;

    float value;
    do {
        ss >> value;
        if(ss.fail()) {
            ss.clear();
            ss >> dummy;
            continue;
        }

        result.push_back(value);
    } while (ss);
}
