#include "ObjParser.h"

void ObjParser::parse(std::string file_path)
{
    std::fstream file;
    file.open(file_path, std::ios::in);
    if(!file.good()) throw std::runtime_error("Could not open file");

    std::string line;
    while (std::getline(file, line)) {
        std::vector<std::string> tokens;
        split(line, tokens);
        if(tokens.empty()) continue;

        if(tokens.at(0) == "v") {
            std::vector<float> elements;
            split_num(line, elements);

            std::array<float, 3> vertex_coords = { elements[0], elements[1], elements[2] };
            vertices.push_back(vertex_coords);
        }
        else if(tokens.at(0) == "f") {
            std::vector<unsigned> face_elements;
            for(std::vector<std::string>::iterator it = std::next(tokens.begin()); it != tokens.end(); ++it) {
                if(*it == "") continue;

                std::vector<std::string> vertex_desc;
                split(*it, vertex_desc, '/');

                std::stringstream converter(vertex_desc.at(0));
                unsigned num;
                converter >> num;

                face_elements.push_back(num - 1);
            }
            faces.push_back(face_elements);
        }
    }

    generate_halfedge();
}

void ObjParser::test()
{
    std::cout << "VERTICES" << std::endl;
    for(auto& vertex: vertices) {
        std::cout << vertex[0] << " " << vertex[1] << " " << vertex[2] << std::endl;
    }

    std::cout << "FACES" << std::endl;
    for(std::vector<unsigned > face: faces) {
        for(int val: face) {
            std::cout << val << " ";
        }
        std::cout << std::endl;
    }

    std::cout << "HE_edges len " << HE_edges.size() << std::endl;
    for(auto &value: HE_edges) {
        std::cout << "V: " << value.second->vertex->index;
        if(value.second->next != nullptr) std::cout << ", next: " << value.second->next->vertex->index;
        if(value.second->pair != nullptr) std::cout << ", pair: " << value.second->pair->vertex->index << "," << value.second->pair->next->vertex->index;
        else std::cout << ", pair: NONE";
        if(value.second->face != nullptr) std::cout << ", face: " << value.second->face;
        std::cout << " edge: " << value.first.first << "," << value.first.second << std::endl;
    }
}