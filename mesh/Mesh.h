#ifndef MODELOWANIE_MESHES_MESH_H
#define MODELOWANIE_MESHES_MESH_H

#include <vector>
#include <algorithm>

struct vertex_t {
    float x;
    float y;
    float z;
};

struct edge_t {
    vertex_t v1;
    vertex_t v2;
};

struct face_t {
    vertex_t v1;
    vertex_t v2;
    vertex_t v3;
};

class Mesh {

public:
//    //3D
//    virtual void swap_edge(face_t& f1, face_t& f2) = 0; //swaps like that: http://i.stack.imgur.com/rbJZC.jpg
//
//    //3F
//    virtual bool mesh_contains_border() = 0; //mesh contains border if there is an edge that belongs to exactly one face
};

#endif //MODELOWANIE_MESHES_MESH_H
