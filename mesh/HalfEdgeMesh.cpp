#include "HalfEdgeMesh.h"

HalfEdgeMesh::HalfEdgeMesh(Parser& parser) : Mesh()
{
    edges = &parser.HE_edges;
    faces = &parser.HE_faces;
    vertices = &parser.vertex_pointers;
}

std::vector<HE_vertex*> HalfEdgeMesh::get_first_layer_of_vertex_surrounding(int vertex_id)
{
    HE_vertex *vertex_ptr = (*vertices)[vertex_id];
    std::vector<HE_vertex*> result;

    HE_edge* edge = vertex_ptr->edge;
    do {
        result.push_back(edge->next->vertex);
        edge = edge->pair->next;
    } while (edge != vertex_ptr->edge);

    return result;
}

std::vector<HE_vertex*> HalfEdgeMesh::get_second_layer_of_vertex_surrounding(int vertex_id)
{
    std::vector<HE_vertex*> result;

    std::vector<HE_vertex*> first_layer = get_first_layer_of_vertex_surrounding(vertex_id);
    for(HE_vertex *first_neighbour: first_layer) {
        HE_edge* edge = first_neighbour->edge;
        do {
            result.push_back(edge->next->vertex);
            edge = edge->pair->next;
        } while (edge != first_neighbour->edge);
    }

    return result;
}

std::vector<HE_face*> HalfEdgeMesh::get_vertex_elements(int vertex_id)
{
    HE_vertex *vertex_ptr = (*vertices)[vertex_id];
    std::vector<HE_face*> result;

    HE_edge* edge = vertex_ptr->edge;
    do {
        result.push_back(edge->face);
        edge = edge->pair->next;
    } while (edge != vertex_ptr->edge);

    return result;
}

std::vector<HE_face*> HalfEdgeMesh::get_first_layer_of_element_surrounding(int element_id)
{
    HE_face* queried_face = nullptr;
    for(HE_face* face: *faces) {
        if(face->id == element_id) {
            queried_face = face;
            break;
        }
    }

    if(queried_face == nullptr) {
        std::cout << "Face with id " << element_id << " does not exist!" << std::endl;
        throw 1;
    }

    std::vector<HE_face*> result;

    HE_edge *edge = queried_face->edge;
    do {
        if(edge->pair != nullptr) {
            HE_face *adjacent = edge->pair->face;
            result.push_back(adjacent);
        }
        edge = edge->next;
    } while (edge != queried_face->edge);

    return result;
}

std::vector<HE_face*> HalfEdgeMesh::get_second_layer_of_element_surrounding(int element_id)
{
    std::vector<HE_face*> first_layer = get_first_layer_of_element_surrounding(element_id);

    std::vector<HE_face*> result;
    for(HE_face* face: first_layer) {
        HE_edge *edge = face->edge;
        do {
            if(edge->pair != nullptr) {
                HE_face *adjacent = edge->pair->face;
                result.push_back(adjacent);
            }
            edge = edge->next;
        } while (edge != face->edge);
    }

    return result;
}

void HalfEdgeMesh::swap_edge(unsigned f1)
{
    HE_face* face1 = this->faces->at(f1);
    HE_face* face2 = face1->edge->pair->face;

    HE_vertex* v2 = face1->edge->next->vertex;
    HE_vertex *v4 = face2->edge->next->vertex;

    face1->edge->vertex = v4;
    face2->edge->vertex = v2;
}

bool HalfEdgeMesh::mesh_contains_border()
{
    bool mesh_contains_border = false;
    typedef std::map<std::pair<int, int>, HE_edge*>::iterator it_type;

    for(it_type iterator = edges->begin(); iterator != edges->end(); iterator++) {
        HE_edge edge = *(*iterator).second;
        if(edge.pair == nullptr)
        {
            mesh_contains_border = true;
            break;
        }
    }
    return mesh_contains_border;
}