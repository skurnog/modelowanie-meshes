#include "VertexIndexMesh.h"

VertexIndexMesh::VertexIndexMesh(Parser& parser) : Mesh()
{
    vertices = &parser.vertices;
    faces = &parser.faces;
}

std::vector<unsigned > VertexIndexMesh::get_first_layer_of_vertex_surrounding(int vertex_id)
{
    std::vector<unsigned > result;
    for(auto it = faces->begin(); it != faces->end(); ++it) {
        std::vector<unsigned> element = *it;
        if(std::find(element.begin(), element.end(), vertex_id) != element.end()) {
            for(unsigned face_vertex: element) {
                if(std::find(result.begin(), result.end(), face_vertex) == result.end() && face_vertex != vertex_id) {
                    result.push_back(face_vertex);
                }
            }
        }
    }

    return result;
}

std::vector<unsigned> VertexIndexMesh::get_second_layer_of_vertex_surrounding(int vertex_id)
{
    std::vector<unsigned> result;
    std::vector<unsigned> first_layer = get_first_layer_of_vertex_surrounding(vertex_id);
    result.insert(result.begin(), first_layer.begin(), first_layer.end());

    for(auto it = first_layer.begin(); it != first_layer.end(); ++it) {
        std::vector<unsigned> second_layer = get_first_layer_of_vertex_surrounding(*it);
        result.insert(result.end(), second_layer.begin(), second_layer.end());
    }

    return result;
}

std::vector<std::vector<unsigned>> VertexIndexMesh::get_vertex_elements(int vertex_id)
{
    std::vector<std::vector<unsigned>> result;
    for(auto it = faces->begin(); it != faces->end(); ++it) {
        std::vector<unsigned> element = *it;
        if(std::find(element.begin(), element.end(), vertex_id) != element.end()) {
            result.push_back(element);
        }
    }

    return result;
}

std::vector<std::vector<unsigned>> VertexIndexMesh::get_first_layer_of_element_surrounding(int element_id)
{
    std::vector<std::vector<unsigned>> result;

    std::vector<unsigned> queried_face = faces->at(element_id);
    for(auto it = faces->begin(); it != faces->end(); ++it) {
        if(it - faces->begin() == element_id) continue;

        for(unsigned face_vertex: *it) {
            if(std::find(queried_face.begin(), queried_face.end(), face_vertex) != queried_face.end()) {
                result.push_back(*it);
                break;
            }
        }
    }

    return result;
}

std::vector<std::vector<unsigned>> VertexIndexMesh::get_second_layer_of_element_surrounding(int element_id)
{
    std::vector<std::vector<unsigned>> result;

    std::vector<std::vector<unsigned>> first_layer = get_first_layer_of_element_surrounding(element_id);
    for(auto queried_face: first_layer) {
        for(auto it = faces->begin(); it != faces->end(); ++it) {

            for(unsigned face_vertex: *it) {
                if(std::find(queried_face.begin(), queried_face.end(), face_vertex) != queried_face.end()) {
                    result.push_back(*it);
                    break;
                }
            }
        }
    }

    return result;
}

////TODO: implement
//void VertexIndexMesh::swap_edge(face f1, face f2)
//{
//
//}
//

bool VertexIndexMesh::mesh_contains_border()
{
    bool mesh_contains_border = false;
    int edge1_appearances = 0;
    int edge2_appearances = 0;
    int edge3_appearances = 0;

    for(auto face1 = faces->begin(); face1 != faces->end(); face1++) {
        int indA1 = (*face1)[0];
        int indA2 = (*face1)[1];
        int indA3 = (*face1)[2];

        for (auto face2 = faces->begin(); face2 != faces->end(); face2++) {

            if(face1 != face2){

                int indB1 = (*face2)[0];
                int indB2 = (*face2)[1];
                int indB3 = (*face2)[2];

                if((indA1 == indB1 && indA2 == indB2 || indA1 == indB2 && indA2 == indB1) ||
                   (indA1 == indB2 && indA2 == indB3 || indA1 == indB3 && indA2 == indB2) ||
                   (indA1 == indB3 && indA2 == indB1 || indA1 == indB1 && indA2 == indB3)) edge1_appearances ++;

                if((indA2 == indB1 && indA3 == indB2 || indA2 == indB2 && indA3 == indB1) ||
                   (indA2 == indB2 && indA3 == indB3 || indA2 == indB3 && indA3 == indB2) ||
                   (indA2 == indB3 && indA3 == indB1 || indA2 == indB1 && indA3 == indB3)) edge2_appearances ++;

                if((indA3 == indB1 && indA1 == indB2 || indA3 == indB2 && indA1 == indB1) ||
                   (indA3 == indB2 && indA1 == indB3 || indA3 == indB3 && indA1 == indB2) ||
                   (indA3 == indB3 && indA1 == indB1 || indA3 == indB1 && indA1 == indB3)) edge3_appearances ++;

            }
        }
        if(edge1_appearances == 0 || edge2_appearances == 0 || edge3_appearances == 0){
            mesh_contains_border = true;
            break;
        }
        edge1_appearances = 0;
        edge2_appearances = 0;
        edge3_appearances = 0;
    }
    return mesh_contains_border;
}

//vertex VertexIndexMesh::convert_to_vertex(std::vector<float> vertex_)
//{
//    float x = vertex_[0];
//    float y = vertex_[1];
//    float z = vertex_[2];
//
//    vertex vert;
//    vert.x = x;
//    vert.y = y;
//    vert.z = z;
//    return vert;
//}