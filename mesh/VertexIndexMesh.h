#ifndef MODELOWANIE_MESHES_VERTEXINDEXMESH_H
#define MODELOWANIE_MESHES_VERTEXINDEXMESH_H

#include "Mesh.h"
#include "../parser/Parser.h"

class VertexIndexMesh : public Mesh{

public:
    VertexIndexMesh(Parser&);

    std::vector<unsigned> get_first_layer_of_vertex_surrounding(int vertex_id);
    std::vector<unsigned > get_second_layer_of_vertex_surrounding(int vertex_id);

    std::vector<std::vector<unsigned>> get_vertex_elements(int vertex_id);

    std::vector<std::vector<unsigned>> get_first_layer_of_element_surrounding(int element_id);
    std::vector<std::vector<unsigned>> get_second_layer_of_element_surrounding(int element_id);

//    void swap_edge(face f1, face f2);
    bool mesh_contains_border();
private:
    std::vector<std::array<float, 3>>* vertices;
    std::vector<std::vector<unsigned>>* faces;

//
//    vertex convert_to_vertex(std::vector<float> vertex_);
};

#endif //MODELOWANIE_MESHES_VERTEXINDEXMESH_H
