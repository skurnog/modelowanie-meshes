#ifndef MODELOWANIE_MESHES_HALFEDGEMESH_H
#define MODELOWANIE_MESHES_HALFEDGEMESH_H

#include "Mesh.h"
#include "../parser/Parser.h"

class HalfEdgeMesh : public Mesh{
public:
    HalfEdgeMesh(Parser&);

    std::vector<HE_vertex*> get_first_layer_of_vertex_surrounding(int vertex_id);
    std::vector<HE_vertex*> get_second_layer_of_vertex_surrounding(int vertex_id);

    std::vector<HE_face*> get_vertex_elements(int vertex_id);

    std::vector<HE_face*> get_first_layer_of_element_surrounding(int element_id);
    std::vector<HE_face*> get_second_layer_of_element_surrounding(int element_id);

    void swap_edge(unsigned f1);

    bool mesh_contains_border();

private:
    std::map<std::pair<int, int>, HE_edge*> *edges;
    std::vector<HE_face*> *faces;
    std::map<int, HE_vertex*> *vertices;
};

#endif //MODELOWANIE_MESHES_HALFEDGEMESH_H
